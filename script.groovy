def buildJar() {
    echo "building the app ..."
    sh 'mvn package'
}

def buildImage() {
    echo "building the docker image ..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-username', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t azataral/my-repo:jma-2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push azataral/my-repo:jma-2.0'
    }
}

def deployApp() {
    echo "deploying the application ..."
}

return this 
